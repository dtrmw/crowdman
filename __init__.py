bl_info = {
	"name": "Crowd Manager",
	"author": "Dharma, DTRMediaWorks",
	"version": (0, 1),
	"blender": (2, 79, 0),
	"description": "Crowd Animation Manager",
	"category": "DTR MediaWorks"
}


import bpy
from bpy.props import (
	StringProperty,
	IntProperty,
	FloatVectorProperty,
	CollectionProperty,
	BoolProperty,
	FloatProperty
)
from bpy.types import (
	Operator,
	Panel,
	PropertyGroup
)

from bpy.app.handlers import persistent

import random

class CrowdAnimationManager(Operator):
	bl_idname = "crowd.animation_manager"
	bl_label = "Crowd Animations"
	bl_options = {'REGISTER'}

	crowdGroup = StringProperty(name="Crowd Group")
	actionToSet = StringProperty(name="Action To Set")
	minOffset = IntProperty(name="Min Offset Frame")
	maxOffset = IntProperty(name="Max Offset Frame")
	repeat = IntProperty(name="Repeat", min=1)
	minScale = FloatProperty(name="Min Scale of Action", min=0, default=1)
	maxScale = FloatProperty(name="Max Scale of Action", min=0, default=1)

	def __init__(self):
		super(CrowdAnimationManager, self).__init__()

	def invoke(self, context, event):
		return context.window_manager.invoke_props_dialog(self, width=500)

	def draw(self, context):
		layout = self.layout
		layout.prop_search(self, "crowdGroup", bpy.data, "groups")
		layout.prop_search(self, "actionToSet", bpy.data, "actions")

		offsetCol = layout.column()
		offsetRow = offsetCol.row()
		offsetRow.prop(self, "minOffset")
		offsetRow.prop(self, "maxOffset")

		layout.prop(self, "repeat")

		scaleCol = layout.column()
		scaleRow = scaleCol.row()
		scaleRow.prop(self, "minScale")
		scaleRow.prop(self, "maxScale")

	def execute(self, context):
		if not self.crowdGroup or not self.actionToSet:
			self.report ({"ERROR"},"Invalid Input")
			return {"CANCELLED"} 
		for proxyRig in bpy.data.groups[self.crowdGroup].objects:
			animData = proxyRig.animation_data
			if not animData:
				animData = proxyRig.animation_data_create()
			nlaTrack = animData.nla_tracks.new()

			startFrame = random.randint(self.minOffset, self.maxOffset)
			nlaStrip = nlaTrack.strips.new("myStrip", startFrame, bpy.data.actions[self.actionToSet])
			nlaStrip.repeat = self.repeat
			nlaStrip.scale = random.uniform(self.minScale, self.maxScale)
		return {"FINISHED"}


class CrowdAgentMovementInfo(PropertyGroup):
	objectName = StringProperty(name="Object")
	startLocation = FloatVectorProperty(name="Start Location")
	moveSpeed = FloatVectorProperty(name="Move Speed")
	referenceFrame = IntProperty(name="Reference Frame", default=0)


class CrowdAgentMovementManager(Operator):
	bl_idname = "crowd.movement_manager"
	bl_label = "Crowd Movements"
	bl_options = {'REGISTER'}

	crowdGroup = StringProperty(name="Crowd Group")
	minSpeed = FloatVectorProperty(name="Min Speed")
	maxSpeed = FloatVectorProperty(name="Max Speed")
	clearPrevData = BoolProperty(name="Clear Previous Data")

	def __init__(self):
		super(CrowdAgentMovementManager, self).__init__()

	def invoke(self, context, event):
		return context.window_manager.invoke_props_dialog(self, width=500)

	def draw(self, context):
		layout = self.layout
		layout.prop_search(self, "crowdGroup", bpy.data, "groups")
		layout.prop(self, "minSpeed")
		layout.prop(self, "maxSpeed")
		layout.prop(self, "clearPrevData")

	def execute(self, context):
		if not self.crowdGroup:
			self.report ({"ERROR"},"Invalid Input")
			return {"CANCELLED"} 
		if self.clearPrevData:
			context.scene.crowd_movement_data.clear()
		for proxyRig in bpy.data.groups[self.crowdGroup].objects:
			cmData = self.getCrowdMomentData(context, proxyRig.name)
			# cmData.objectName = proxyRig.name
			cmData.startLocation = proxyRig.location
			cmData.moveSpeed = [
				random.uniform(self.minSpeed[0], self.maxSpeed[0]),
				random.uniform(self.minSpeed[1], self.maxSpeed[1]),
				random.uniform(self.minSpeed[2], self.maxSpeed[2])
			]
			cmData.referenceFrame = bpy.context.scene.frame_current
		return {"FINISHED"}

	def getCrowdMomentData(self, context, proxyName):
		for cmData in context.scene.crowd_movement_data:
			if cmData.objectName == proxyName:
				print ("CMData found {0}".format(proxyName))
				return cmData
		newCMData = context.scene.crowd_movement_data.add()
		newCMData.objectName = proxyName
		return newCMData


def getCurrentCMData(context):
	cmData = None
	try:
		cmData = context.scene.crowd_movement_data[context.scene.crowd_data_index]
	except Exception as e:
		pass
	return cmData

def cmDataSelected(self, context):
	cmData = getCurrentCMData(context)
	if not cmData:
		return
	cAgentObj = bpy.data.objects[cmData['objectName']]
	bpy.ops.object.select_all(action="DESELECT")
	cAgentObj.select = True

@persistent
def calculateAgentPosition(scene):
	for cmData in bpy.context.scene.crowd_movement_data:
		cObj = bpy.data.objects[cmData.objectName]
		currentFrame = bpy.context.scene.frame_current
		# as reference frame introduced newly, 'refFrame' is just a work around not to break		
		refFrame = 0
		try:
			refFrame = cmData["referenceFrame"]
		except Exception as e:
			refFrame = 0
		currentLocation = [
			cmData['startLocation'][0] + cmData["moveSpeed"][0] * (currentFrame - refFrame),
			cmData['startLocation'][1] + cmData["moveSpeed"][1] * (currentFrame - refFrame),
			cmData['startLocation'][2] + cmData["moveSpeed"][2] * (currentFrame - refFrame)
		]
		cObj.location = currentLocation

def computeEachFrameUpdated(self, context):
	if context.scene.crowd_compute_each_frame:
		registerHandler()
	else:
		removeHandler()

def registerHandler():
	handler = bpy.app.handlers.frame_change_pre
	if not calculateAgentPosition in handler:
		handler.append(calculateAgentPosition)

def removeHandler():
	handler = bpy.app.handlers.frame_change_pre
	if calculateAgentPosition in handler:
		handler.remove(calculateAgentPosition)


class ListItems(bpy.types.UIList):
	"""docstring for ListItems"""
	def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
		layout.label(item['objectName'])

class ListItemsActions(bpy.types.Operator):
	bl_idname = "crowds.crowds_list_action"
	bl_label = "Actions for Crowd Agene Data"

	action = bpy.props.EnumProperty(
		items=(
			('CMD_REMOVE', "Remove Movement Data", ""),
			('CMD_ADD', "Add Movement Data", "")
		)
	)

	def invoke(self, context, event):
		cmData = getCurrentCMData(context)
		if not cmData:
			return {"CANCELLED"}

		if cmData and self.action == 'CMD_ADD':
			pass
		elif cmData and self.action == 'CMD_REMOVE':
			context.scene.crowd_movement_data.remove(context.scene.crowd_data_index)
		return {"FINISHED"}


class CrowdsPanel(Panel):
	bl_idname = "crowds.panel"
	bl_space_type = "VIEW_3D"
	bl_region_type = "TOOLS"
	bl_label = "Crowd Manager"
	bl_description = ""
	bl_category = "DTR MediaWorks"

	def draw(self, context):
		layout = self.layout
		layout.operator(CrowdAnimationManager.bl_idname)
		layout.operator(CrowdAgentMovementManager.bl_idname)
		layout.prop(context.scene, "crowd_compute_each_frame")

		listColumn = layout.column()
		listColumn.template_list("ListItems",
			"",
			bpy.context.scene,
			"crowd_movement_data",
			bpy.context.scene,
			"crowd_data_index"
			)
		listRow = listColumn.row()
		listRow.operator(ListItemsActions.bl_idname, text="Add").action = "CMD_ADD"
		listRow.operator(ListItemsActions.bl_idname, text="Remove").action = "CMD_REMOVE"
		
		cmData = getCurrentCMData(context)
		if cmData:
			layout.prop(cmData, "objectName")
			layout.prop(cmData, "moveSpeed")
			layout.prop(cmData, "startLocation")



def register():
	bpy.utils.register_class(CrowdAnimationManager)
	bpy.utils.register_class(CrowdAgentMovementManager)
	bpy.utils.register_class(CrowdAgentMovementInfo)
	bpy.utils.register_class(ListItems)
	bpy.utils.register_class(ListItemsActions)
	bpy.utils.register_class(CrowdsPanel)
	bpy.types.Scene.crowd_movement_data = CollectionProperty(type=CrowdAgentMovementInfo)
	bpy.types.Scene.crowd_data_index = IntProperty(update=cmDataSelected)
	bpy.types.Scene.crowd_compute_each_frame = BoolProperty(name="Compute at each Frame", update=computeEachFrameUpdated)
	registerHandler()

def unregister():
	bpy.utils.unregister_class(CrowdAnimationManager)
	bpy.utils.unregister_class(CrowdAgentMovementManager)
	bpy.utils.unregister_class(CrowdAgentMovementInfo)
	bpy.utils.unregister_class(ListItems)
	bpy.utils.unregister_class(ListItemsActions)
	bpy.utils.unregister_class(CrowdsPanel)
	del bpy.types.Scene.crowd_movement_data
	del bpy.types.Scene.crowd_data_index
	del bpy.types.Scene.crowd_compute_each_frame
	removeHandler()

if __name__ == "__main__":
	register()
